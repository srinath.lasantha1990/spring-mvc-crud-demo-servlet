package com.springdemo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.springdemo.entity.Customer;

@Repository
public interface CustomerDAO {

	List<Customer> getCustomers();

	void saveCustomer(Customer customer);
}
